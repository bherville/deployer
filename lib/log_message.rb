class LogMessage
  attr_accessor :message, :level

  def initialize(message = nil, level = :info, callback)
    @message, @level = message, level
    @callback = callback
  end

  def update(message, level)
    @message = message
    @level = level
    @callback.call(self)
  end
end