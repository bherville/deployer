require 'git'
require 'logger'
require 'fileutils'
require 'time'
require 'observer'
require 'log_message'

class Deployment
  include Observable

  def initialize(deploy_destination, repository, version, name, logger, working_copy_dir, observers = [])
    @deploy_destination = deploy_destination
    @repository = repository
    @version = version
    @name = name
    @logger = logger
    @working_copy_dir = working_copy_dir

    setup observers
  end

  def setup(observers)
    observers.each do |observer|
      add_observer observer
    end

    @working_copy = prepare_working_copy @working_copy_dir
  end

  def prepare_working_copy (working_copy)
    time_stamp = Time.now.strftime('%Y-%m-%d%H_%M_%S')

    log "Preparing the working copy #{working_copy}", :info

    if File.directory? working_copy
      log "A directory with the same name as the working copy, #{working_copy}, already exists moving directory to #{working_copy}-#{time_stamp}.bak", :info

      FileUtils.mv(working_copy, File.join(File.expand_path("..",working_copy), "#{File.basename(working_copy)}-#{time_stamp}.bak"))
    end

    log "Creating working copy #{working_copy}", :info
    Dir.mkdir working_copy

    log "Cloning repository #{@repository}", :info
    wc = Git.clone(@repository, working_copy)

    return wc
  end

  def update (tag)
    log "Updating destination working copy #{@deploy_destination}", :info
    if !File.directory? @deploy_destination
      log 'Destination does not exists, creating and cloning repository', :info

      Dir.mkdir @deploy_destination
      Git.clone @repository, @deploy_destination, :path => File.expand_path("..", @deploy_destination)
    end

    log 'Opening destination working copy', :info
    wc = Git.open(@deploy_destination, :log => @logger)


    log 'Fetching', :info
    wc.fetch
    log 'Pulling', :info
    wc.pull
    log "Checking out tag #{tag}", :info
    wc.checkout tag
  end

  def version?(tag)
    #Check to see if tag already exists
    begin
      @working_copy.tag(tag)
      return true
    rescue
      return false
    end
  end

  def delete_version(tag)
    @working_copy.delete_tag(tag)
    @working_copy.push 'origin', ":refs/tags/#{tag}"
  end

  #Reset the working copy to master.
  def reset(force = false)
    if File.directory? @deploy_destination
      log 'Opening destination working copy', :info
      wc = Git.open(@deploy_destination, :log => @logger)


      wc.fetch('origin')
      if force
        log 'Force requested, Resetting Hard', :info
        wc.reset_hard('origin/master')
      else
        log 'Force not requested, Resetting', :info
        wc.reset
      end

      wc.checkout('master')

    else
      log 'Destination does not exists, creating and cloning repository', :info

      Dir.mkdir @deploy_destination
      Git.clone @repository, @deploy_destination
    end


  end

  def deploy (deploy_source, force = false, update_destination = true)
    log 'Deploying...', :info

    #Copy the files from the deploy_source into the Git clone working directory
    log 'Copying files into working copy', :info
    FileUtils.cp_r "#{deploy_source}/.", @working_copy_dir, :remove_destination=>true


    #Stage the new files to the Git clone's revision control
    log 'Adding new files to git in the working copy', :info
    @working_copy.add


    #Attempt to commit the changes the new staged changes into Git revision control
    begin
      log 'Committing changes', :info
      @working_copy.commit_all("Updated: Release #{@version}")
    rescue Exception => e
      #If this was not successful then there was probably no changes in the deploy_source
      log 'No files/directories new were detected, not committing', :warn

      #Raise the Exception e if force is false, otherwise continue running this method
      if !force
        raise e
      end
    end

    #Grab the tag that should be associated with this new revision
    tag = @version
    log "Creating tag #{tag}", :info

    #Check to see if tag already exists
    if version? tag
      @working_copy.tag(tag)

      log "Tag #{tag} already exists, not creating.", :info

    else
      @working_copy.add_tag(tag)
    end

    #Try to push the changes into the remote Git repository
    log "Pushing updates to the origin master with the tag: #{tag}", :info
    begin
      @working_copy.push 'origin', 'master', true
      log 'Push was successful.', :info
    rescue Exception => e
      log "Exception encountered: #{e}. Attempting a pull.", :info
      begin
        @working_copy.pull('origin', 'master')
        log 'Pull was successful.', :info
      rescue Exception => e1
        log "Exception encountered: #{e1}.", :info
        raise e1
      end
    end

    if update_destination
      log "Update Destination was set to: #{update_destination}. Running Update.", :info
      update @version
    end
  end

  private
  def log(message, level = :info)
    case level
      when :info
        @logger.info(message)
      when :warn
        @logger.warn(message)
      when :error
        @logger.error(message)
      when :fatal
        @logger.fatal(message)
      when :debug
    end

    changed
    notify_observers(message, level)
  end
end